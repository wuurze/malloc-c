# malloc-c

A personal implementation of the malloc(), calloc(), realloc(), and free() functions vital in the C programming language. These are accomplished by manipulating a free list.